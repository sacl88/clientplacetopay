import { Injectable } from '@angular/core';
import { Bank } from './bank';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class BanksService {

  constructor(private httpClient:HttpClient) { }

  getBanks(): Observable<Bank>{
  	return this.httpClient.get(`${environment.api_url}banks`)
  	.catch((error:any) => Observable.throw(error.error || {message:"Error en el servidor"}))
  }

}
