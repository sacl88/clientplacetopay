import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { routes } from './app.routes';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BanksComponent } from './banks/banks.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { BanksService } from './banks/banks.service';
import { TransactionsService } from './transactions/transactions.service';
import { TransactionsComponent } from './transactions/transactions.component';
import { TransactionsListComponent } from './transactions/transactions-list/transactions-list.component';
import { TransactionThanksComponent } from './transactions/transaction-thanks/transaction-thanks.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    BanksComponent,
    TransactionsComponent,
    TransactionsListComponent,
    TransactionThanksComponent
  ],
  imports: [
  	routes,
    BrowserModule,    
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
  	BanksService,
    TransactionsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
