import { Component, OnInit } from '@angular/core';
import { Bank } from '../banks/bank';
import { Location } from '@angular/common';
import { DOCUMENT } from '@angular/platform-browser';
import { BanksService } from '../banks/banks.service';
import { TransactionsService } from '../transactions/transactions.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { environment } from './../../environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

	banks: Bank;

	f: FormGroup;
	message:any;
  loading:any;
  redirectToBank:any;
  loadBanks:any = true;
  messageCheckAllTransactions:any = false;
  successCheckAllTransactions:any = false;

  	constructor(
  		private _location: Location,
		  private formBuilder: FormBuilder,
  		private bankService: BanksService,
  		private transactionService: TransactionsService,
  		private router: Router
  	) { }

  	ngOnInit() {

      this.transactionService.checkTransactionsAll().subscribe(
        (resp) => {
          this.messageCheckAllTransactions = resp["message"];
          this.successCheckAllTransactions = resp["success"];
        }
      )


  		this.f = this.formBuilder.group({
  			bankInterface: [null, [Validators.required]],		
  			bankCode: [0, [Validators.required]],
  			description: ["descripcion de prueba", [Validators.required]],
  			totalAmount: [4500, [Validators.required]],
  			taxAmount: [0, [Validators.required]],
  			devolutionBase: [0, [Validators.required]],
  			tipAmount: [0, [Validators.required]],
  			documentType: [null, [Validators.required]],
  			document: [1098671037, [Validators.required]],
  			firstName: ["Sergio Andres", [Validators.required]],
  			lastName: ["Celis León", [Validators.required]],
  			company: ["Modamel", [Validators.required]],
  			emailAddress: ["sergioleon1127@gmail.com", [Validators.required, Validators.email]],
  			address: ["calle 60 # 7 w 44 Mutis", [Validators.required]],
  			city: [null, [Validators.required]],
  			province: [null, [Validators.required]],
  			country: [null, [Validators.required]],
  			phone: ["0576445577", [Validators.required]],
  			mobile: ["3142547434", [Validators.required]],
  		})

  		this.bankService.getBanks().subscribe(
  			(resp) => {
          if(typeof resp["getBankListResult"] != "undefined"){
            if(typeof resp["getBankListResult"].item != "undefined"){
              this.banks = resp["getBankListResult"].item;
            }else{
              this.loadBanks = false;
            }  				          
          }else{
            this.loadBanks = false;
          }
  			}
  		)
  	}

  	createTransaction(){
      this.loading = true;      
  		this.f.value.returnURL = environment.base_url+"/thanks";
  		this.transactionService.addTransaction(this.f.value).subscribe(
  			(resp) => {
  				if(resp["success"]){
            this.loading = false;
            this.redirectToBank = true;
  					window.location.href = resp["transaction"].bankURL;
  				}
  			}
  		)
	}

}
