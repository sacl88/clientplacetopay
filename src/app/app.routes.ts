import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from "@angular/core";
import { DashboardComponent } from './dashboard/dashboard.component';
import { TransactionsListComponent } from './transactions/transactions-list/transactions-list.component';
import { TransactionThanksComponent } from './transactions/transaction-thanks/transaction-thanks.component';

const appRoutes: Routes = [
	{ path: '', redirectTo:'/dashboard', pathMatch: 'full' },
	{ path: 'dashboard', component: DashboardComponent },
	{ path: 'transactions-list', component: TransactionsListComponent },	
	{ path: 'thanks', component: TransactionThanksComponent },
];

export const routes:ModuleWithProviders = RouterModule.forRoot(appRoutes);