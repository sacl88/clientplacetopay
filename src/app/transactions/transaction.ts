export class Transaction {
	constructor(
		public transactionID:string,
		public sessionID:string,
		public returnCode:string,
		public trazabilityCode:string,
		public transactionCycle:string,
		public bankCurrency:string,
		public bankFactor:string,
		public bankURL:string,
		public responseCode:string,
		public responseReasonCode:string,
		public responseReasonText:string,
		public transactionState:string,
	){

	}
}