import { Injectable } from '@angular/core';
import { Transaction } from './transaction';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class TransactionsService {

  	constructor(private httpClient:HttpClient) { }

  	check(): boolean{
	  	return localStorage.getItem('transaction') ? true : false;
  	}

  	getTransaction(): Transaction {
  		return localStorage.getItem('transaction') ? JSON.parse(atob(localStorage.getItem('transaction'))) : null;
  	}

	  getTransactions(): Observable<Transaction>{
  		return this.httpClient.get(`${environment.api_url}transactions`)
  		.catch((error:any) => Observable.throw(error.error || {message:"Error en el servidor"}))
  	} 

  	addTransaction(transaction: Object): Observable<Transaction[]>{
	  	return this.httpClient.post(`${environment.api_url}create-transaction`, transaction)
	  	.do(data => {
	      	localStorage.setItem('transaction', btoa(JSON.stringify(data["transaction"])));
	  	})
	  	.catch((error:any) => Observable.throw(error.error || {message:"Error en el servidor"}))
  	}

  	checkTransactions(transactionID): Observable<Transaction>{
  		return this.httpClient.get(`${environment.api_url}check-transaction/` + transactionID)
  		.catch((error:any) => Observable.throw(error.error || {message:"Error en el servidor"}))
  	} 

    checkTransactionsAll(): Observable<any>{
      return this.httpClient.get(`${environment.api_url}check-transaction-all`)
      .catch((error:any) => Observable.throw(error.error || {message:"Error en el servidor"}))
    } 

}
