import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionThanksComponent } from './transaction-thanks.component';

describe('TransactionThanksComponent', () => {
  let component: TransactionThanksComponent;
  let fixture: ComponentFixture<TransactionThanksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionThanksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionThanksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
