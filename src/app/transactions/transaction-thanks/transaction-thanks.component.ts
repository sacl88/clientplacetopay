import { Component, OnInit } from '@angular/core';
import { environment } from './../../../environments/environment';
import { TransactionsService } from '../../transactions/transactions.service';
import { Router } from '@angular/router';
import { Transaction } from '../transaction';

@Component({
  selector: 'app-transaction-thanks',
  templateUrl: './transaction-thanks.component.html',
  styleUrls: ['./transaction-thanks.component.css']
})
export class TransactionThanksComponent implements OnInit {

	transaction:Transaction

  	constructor(private transactionService: TransactionsService, private router: Router) { }

  	ngOnInit() {
  		if(this.transactionService.check()){  			
  			var transaction = this.transactionService.getTransaction();
  			this.transactionService.checkTransactions(transaction.transactionID).subscribe(
				(resp) => {
					this.transaction = resp;	
					if(this.transaction.transactionState != "OK"){
						localStorage.clear();						
					}
          if(this.transaction.transactionState != "PENDING"){
            localStorage.clear();            
          }									
				}
			)
  		}else{
  			this.router.navigate(['dashboard']);
  		}  		
  	}

}
