import { Component, OnInit } from '@angular/core';
import { TransactionsService } from '../transactions.service';
import { Transaction } from '../transaction';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'app-transactions-list',
  templateUrl: './transactions-list.component.html',
  styleUrls: ['./transactions-list.component.css']
})
export class TransactionsListComponent implements OnInit {
	transactions: Transaction;
  messageCheckAllTransactions:any = false;
  successCheckAllTransactions:any = false;
  loading:any = true;

	constructor(private transactionService: TransactionsService,) { }

	ngOnInit() {
    this.loading = true;
    this.transactionService.checkTransactionsAll().subscribe(
      (resp) => {
        this.messageCheckAllTransactions = resp["message"];
        this.successCheckAllTransactions = resp["success"];
        this.loading = false;
      }
    )

		this.transactionService.getTransactions().subscribe(
			(resp) => {
  				this.transactions = resp["data"];
			}
		)
	}

}
